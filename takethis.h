/*
        Copyright 2017 Yarl Baudig

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _TAKETHIS_H
#define _TAKETHIS_H

#include <pthread.h>
#include <sys/queue.h>
#include <stdio.h>
#include <error.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>

#define DECLARE_TAKETHIS(name,type) \
\
typedef struct takethis_##name##_##type##_t{\
  type* value;\
  TAILQ_ENTRY(takethis_##name##_##type##_t) next;\
} takethis_##name##_##type##_t;\
typedef void (*takethis_##name##_consumer_t)(type*); \
\
typedef struct takethis_##name##_t{\
  pthread_mutex_t mutex;\
  pthread_cond_t cond;\
  TAILQ_HEAD(takethis_##name##_tq,takethis_##name##_##type##_t) head;\
  takethis_##name##_consumer_t consumer;\
  pthread_t consumer_tid;\
} takethis_##name##_t;\
\
void takethis_##name##_init(takethis_##name##_t* takethis, takethis_##name##_consumer_t consumer);\
void takethis_##name##_snd(takethis_##name##_t* takethis, type* value);

#define DEFINE_TAKETHIS(name, type) \
\
static void* \
consumer_##name##_wrap(void* arg)\
{\
  takethis_##name##_t *takethis = (takethis_##name##_t*) arg;\
  takethis_##name##_##type##_t* _value = NULL;\
  type* value = NULL;\
  int ret = 0;\
  while(1){\
    if((ret = pthread_mutex_lock(&takethis->mutex))){\
      fprintf(stderr, "%s pthread_mutex_lock error %d\n", __func__, ret);\
      exit(EXIT_FAILURE);\
    }\
    while(TAILQ_EMPTY(&takethis->head)){\
      if((ret=pthread_cond_wait(&takethis->cond, &takethis->mutex))){\
        fprintf(stderr, "%s pthread_cond_wait error %d\n", __func__, ret);\
        exit(EXIT_FAILURE);\
      }\
    }\
    while((_value = TAILQ_LAST(&takethis->head, takethis_##name##_tq))){\
      TAILQ_REMOVE(&takethis->head, _value, next);\
      value = _value->value;\
      free(_value);\
      takethis->consumer(value);\
    }\
    if((ret=pthread_mutex_unlock(&takethis->mutex))){\
      fprintf(stderr, "%s pthread_mutex_unlock error %d\n", __func__, ret);\
      exit(EXIT_FAILURE);\
    }\
  }\
  return NULL;\
}\
\
void takethis_##name##_init(takethis_##name##_t* takethis, takethis_##name##_consumer_t consumer)\
{\
  pthread_mutex_init(&takethis->mutex, NULL);\
  pthread_cond_init(&takethis->cond, NULL);\
  TAILQ_INIT(&takethis->head);\
  takethis->consumer = consumer;\
  sigset_t set, oldset;\
  if(sigfillset(&set))\
    error(1,errno,"sigfillset");\
  if(pthread_sigmask(SIG_SETMASK, &set, &oldset))\
    error(1,errno,"pthread_sigmask");\
  if(pthread_create(&takethis->consumer_tid, NULL, consumer_##name##_wrap, takethis))\
    error(1,0,__func__);\
if(pthread_sigmask(SIG_SETMASK, &oldset, NULL))\
    error(1,errno,"pthread_sigmask");\
}\
\
void takethis_##name##_snd(takethis_##name##_t *takethis, type* value)\
{\
  takethis_##name##_##type##_t* _value = malloc(sizeof(*_value));\
  int ret=0;\
  if(!_value){\
    fprintf(stderr, "%s: error, out of memory\n", __func__);\
    exit(EXIT_FAILURE);\
  }\
  _value->value = value;\
  if((ret=pthread_mutex_lock(&takethis->mutex))){\
    fprintf(stderr, "%s pthread_mutex_lock error %d\n", __func__, ret);\
    exit(EXIT_FAILURE);\
  }\
  TAILQ_INSERT_HEAD(&takethis->head, _value, next);\
  if((ret=pthread_cond_signal(&takethis->cond))){\
    fprintf(stderr, "%s pthread_cond_signal error %d\n", __func__, ret);\
    exit(EXIT_FAILURE);\
  }\
  if((ret=pthread_mutex_unlock(&takethis->mutex))){\
    fprintf(stderr, "%s pthread_mutex_unlock error %d\n", __func__, ret);\
    exit(EXIT_FAILURE);\
  }\
}

#endif // _TAKETHIS_H
